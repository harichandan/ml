import math

class Vector(object):
    def __init__(self, coordinates):
        try:
            if not coordinates:
                raise ValueError
            self.coordinates = tuple(coordinates)
            self.dimension = len(coordinates)

        except ValueError:
            raise ValueError('The coordinates must be nonempty')

        except TypeError:
            raise TypeError('The coordinates must be an iterable')

    def add(self, v):
        return Vector([x+y for x,y in zip(self.coordinates, v.coordinates)])

    def subtract(self, v):
        return Vector([x-y for x,y in zip(self.coordinates, v.coordinates)])

    def scalar_product(self, num):
        return Vector([num*x for x in self.coordinates])

    def magnitude(self):
        return math.sqrt(sum([x**2 for x in self.coordinates]))

    def normalize(self):
        try:
            magnitude = self.magnitude()
            return self.scalar_product(1./magnitude)
        except ZeroDivisionError:
            raise Exception('Magnitude shouldn\'t be zero')

    def dot(self, v):
        return sum([x*y for x,y in zip(self.coordinates, v.coordinates)])

    def angle_rad(self, v):
        return math.acos(self.normalize().dot(v.normalize()))

    def angle_degrees(self, v):
        return math.degrees(self.angle_rad(v))
    def __str__(self):
        return '{}'.format(self.coordinates)


    def __eq__(self, v):
        return self.coordinates == v.coordinates

v = Vector([1,2,3])
w = Vector([2,3,4])
print("{} + {} = ".format(v, w), v.add(w))
print("{} - {} = ".format(v, w), v.subtract(w))
print("{} * 3 = ".format(v), v.scalar_product(3))
print("Magnitude of {}: ".format(v), v.magnitude())
print("Normalize {}: ".format(v), v.normalize())
print(v.dot(w))
print(v.angle_degrees(w))